#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  // Delete zeros on the left of `lhs` and `rhs`
  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  // Compute strings lenght of `lhs` and `rhs`
  size_t len_lhs = strlen(lhs), len_rhs = strlen(rhs);

  // Initialize variables for addition
  size_t sum, digit;
  unsigned int rest = 0;
  size_t max_len = (len_lhs > len_rhs) ? len_lhs : len_rhs;
  size_t result_index = max_len + 1;
  
  // Allocate memory to store the result, 1 for NULL and 1 for the rest
  char *result = calloc(max_len + 2, sizeof(char));

  // Parse the strings `lhs` and `rhs` from right to left, Addition bit per bit and store the result in `result`
  while (len_lhs > 0 || len_rhs > 0 || rest != 0) {
    // Récupérer les valeurs des chiffres à ajouter
    size_t lhs_digit = (len_lhs > 0 ? get_digit_value(lhs[--len_lhs]) : 0);
    size_t rhs_digit = (len_rhs > 0 ? get_digit_value(rhs[--len_rhs]) : 0);

    // Display operation details if verbose is enabled
    if (VERBOSE) {
      fprintf(stderr, "add: digit %c digit %c carry %u\n", to_digit(lhs_digit), to_digit(rhs_digit), rest);
    }

    // Compute the sum and the rest
    sum = lhs_digit + rhs_digit + rest;
    rest = sum / base;
    digit = sum % base;

    // Display operation details if verbose is enabled
    if (VERBOSE) {
      fprintf(stderr, "add: result: digit %c carry %u\n", to_digit(digit), rest);
    }

    // Store the result in `result`
    result[--result_index] = to_digit(digit);
  }

  // Display the final rest if there is one and if verbose mode is enabled
  if (rest != 0 && VERBOSE) {
    fprintf(stderr, "add: final carry %u\n", rest);
  }

  // Return `result` by advancing the index of the first non-zero value
  return result + result_index;
}


char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  // Check if lhs == rhs, if yes, the result is 0
  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);
  if (strcmp(lhs, rhs) == 0) {
    char *result = malloc(2);
    result[0] = '0';
    result[1] = '\0';
    return result;
  }
  
  // Check if rhs == 0, if yes, the result is lhs
  else if (strcmp(rhs, "0") == 0) {
    return strdup(lhs);
  }

  // Check if lhs > rhs, otherwise return NULL
  size_t len_lhs = strlen(lhs), len_rhs = strlen(rhs);
  if (len_lhs < len_rhs || (len_lhs == len_rhs && strcmp(lhs, rhs) < 0)) {
    return NULL;
  }

  // Initialisation
  size_t max_len = (len_lhs > len_rhs) ? len_lhs : len_rhs;
  int rest = 0;
  int result_index = max_len - 1;
  char *result = malloc(max_len + 2); 
  result[result_index + 1] = '\0';

  // Parse the strings `lhs` and `rhs` from right to left, Substract bit per bit and store the result in `result`
  while (len_lhs > 0 || len_rhs > 0) {
    size_t lhs_digit = (len_lhs > 0 ? get_digit_value(lhs[--len_lhs]) : 0);
    size_t rhs_digit = (len_rhs > 0 ? get_digit_value(rhs[--len_rhs]) : 0);
    if (VERBOSE) {
      fprintf(stderr, "sub: digit %c digit %c carry %u\n", to_digit(lhs_digit), to_digit(rhs_digit), rest);
    }
    int difference = lhs_digit - rhs_digit - rest + base;
    rest = (difference >= base ? 0 : 1);
    size_t digit = difference % base;
    if (VERBOSE) {
      fprintf(stderr, "sub: result: digit %c carry %u\n", to_digit(digit), rest);
    }
    result[result_index--] = to_digit(digit);
  }

  // Handling the remaining rest
  while (result[result_index + 1] == '0') {
    ++result_index;
  }

  // if the result is NULL, return NULL, otherwise return the result
  if (result_index == max_len - 1) { 
    return NULL;
  } else { 
    return result + result_index + 1;
  }
}


char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  // Delete the extra zeros from both operands
  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  // If one of the two operands is "0", the result is "0".  
  if (strcmp(lhs, "0") == 0 || strcmp(rhs, "0") == 0) {
    return "0";
  }

  // Calculate the length of each operand  
  size_t len_lhs = strlen(lhs), len_rhs = strlen(rhs);

  // Calculate the maximum length of the result
  size_t max_len = len_lhs + len_rhs;

  // Allocate memory for the result as an array of unsigned integers
  unsigned int *result = calloc(max_len, sizeof(unsigned int));

  // Perform the multiplication digit by digit, using a double loop
  for (size_t i = 0; i < len_lhs; i++) {
    unsigned int carry = 0;
    unsigned int digit_lhs = get_digit_value(lhs[len_lhs - 1 - i]);
    for (size_t j = 0; j < len_rhs; j++) {
      unsigned int digit_rhs = get_digit_value(rhs[len_rhs - 1 - j]);
      unsigned int sum = result[i + j] + digit_lhs * digit_rhs + carry;
      result[i + j] = sum % base;
      carry = sum / base;
    }
    if (carry > 0) {
      result[i + len_rhs] += carry;
    }
  }

  // Remove excess zeros from the result
  size_t result_len = max_len;
  while (result_len > 1 && result[result_len - 1] == 0) {
    result_len--;
  }

  // Convert the array of unsigned integers into a string
  char *result_str = calloc(result_len + 1, sizeof(char));
  for (size_t i = 0; i < result_len; i++) {
    result_str[result_len - 1 - i] = to_digit(result[i]);
  }
  result_str[result_len] = '\0';

  return result_str;
}


// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
