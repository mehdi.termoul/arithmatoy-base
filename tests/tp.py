def nombre_entier(n: int) -> str:
    """
    Returns a string representing the natural number n in the unary numeral system.

    Parameters:
    n (int): The non-negative integer to be converted to a unary string representation.

    Returns:
    str: The unary string representation of n, where each "S" character represents one unit and a final "0" represents the end of the number.
    """
    if n == 0:
        return "0"
    else:
        return ("S" * n) + "0"


def S(n: str) -> str:
    """
    Returns a unary string representation of the successor of the natural number represented by the input string.

    Parameters:
    n (str): The unary string representation of a natural number.

    Returns:
    str: The unary string representation of the successor of the natural number represented by n, obtained by adding one "S" character to the beginning of the string.
    """
    return "S" + n


def addition(a: str, b: str) -> str:
    """
    Returns a unary string representation of the sum of two natural numbers represented by input unary strings.

    Parameters:
    a (str): The unary string representation of the first natural number to be added.
    b (str): The unary string representation of the second natural number to be added.

    Returns:
    str: The unary string representation of the sum of a and b.
    """
    if a == "0":
        return b
    elif b == "0":
        return a
    else:
        return S(addition(a[1:], b))


def multiplication(a: str, b: str) -> str:
    """
    Returns a unary string representation of the product of two natural numbers represented by input unary strings.

    Parameters:
    a (str): The unary string representation of the first natural number to be multiplied.
    b (str): The unary string representation of the second natural number to be multiplied.

    Returns:
    str: The unary string representation of the product of a and b.
    """
    if a == "0":
        return a
    elif b == "0":
        return b
    else:
        return nombre_entier(len(a.strip("0")) * len(b.strip("0")))


def facto_ite(n: int) -> int:
    """
    Computes the factorial of a non-negative integer using an iterative algorithm.

    Parameters:
    n (int): The non-negative integer to compute the factorial of.

    Returns:
    int: The factorial of n, defined as the product of all positive integers up to and including n.
    """
    factorial = 1
    for i in range(1, n + 1):
        factorial *= i
    return factorial


def facto_rec(n: int) -> int:
    """
    Computes the factorial of a non-negative integer using a recursive algorithm.

    Parameters:
    n (int): The non-negative integer to compute the factorial of.

    Returns:
    int: The factorial of n, defined as the product of all positive integers up to and including n.
    """
    if n == 1 or n == 0:
        return 1
    else:
        return n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    """
    Computes the nth Fibonacci number using a recursive algorithm.

    Parameters:
    n (int): The non-negative integer representing the position of the Fibonacci number to compute.

    Returns:
    int: The nth Fibonacci number, defined as the sum of the (n-1)th and (n-2)th Fibonacci numbers.
    """
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo_rec(n - 1) + fibo_rec(n - 2)


def fibo_ite(n: int) -> int:
    """
    Computes the nth Fibonacci number using an iterative algorithm.

    Parameters:
    n (int): The non-negative integer representing the position of the Fibonacci number to compute.

    Returns:
    int: The nth Fibonacci number, defined as the sum of the (n-1)th and (n-2)th Fibonacci numbers.
    """
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        a, b = 0, 1
        for i in range(2, n + 1):
            a, b = b, a + b
        return b


def golden_phi(n: int) -> int:
    """
    Computes the nth number in the golden ratio sequence using an iterative algorithm.

    Parameters:
    n (int): The non-negative integer representing the position of the number to compute in the golden ratio sequence.

    Returns:
    int: The nth number in the golden ratio sequence, defined as the ratio of the (n+1)th and nth Fibonacci numbers.
    """
    return fibo_ite(n + 1) / fibo_ite(n)


def sqrt5(n: int) -> int:
    """
    Computes the nth approximation of the square root of 5 using the golden ratio.

    Parameters:
    n (int): The non-negative integer representing the position of the approximation to compute.

    Returns:
    int: The nth approximation of the square root of 5, obtained by dividing the (n+1)th number in the golden ratio sequence by 2.
    """
    return 2 * golden_phi(n) - 1


def pow(a: float, n: int) -> float:
    """
    Returns the value of a raised to the power of n.

    Parameters:
    a (float): Base value.
    n (int): Exponent value.

    Returns:
    float: Value of a raised to the power of n.
    """
    return a**n
